import { Component, Input, OnInit } from '@angular/core';
import { MeteoProvider } from "../../providers/meteo/meteo";

/**
 * Generated class for the MeteoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'meteo',
  templateUrl: 'meteo.html'
})
export class MeteoComponent implements OnInit{
  @Input() ville: string;
  title: string;
  urlImg: string;
  lat: string;
  lon: string;
  todate: string;
  condition: string;
  tmp: string;

  constructor(
    private meteoProvider: MeteoProvider
  ) {
    console.log('Hello MeteoComponent Component');
  }

  ngOnInit() {
    this.title = this.ville;
    var that = this;
    this.meteoProvider.getMeteo(this.ville).then(function (data) {
      console.log(data);
      that.lat = data['city_info'].latitude;
      that.lon = data['city_info'].longitude;
      that.urlImg = data['current_condition'].icon_big;
      that.todate = data['current_condition'].date;
      that. condition = data['current_condition'].condition;
      that.tmp = data['current_condition'].tmp;
    });
  }

}
