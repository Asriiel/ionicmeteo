import { NgModule } from '@angular/core';
import { Component } from '@angular/core';
import { MeteoComponent } from './meteo/meteo';
@NgModule({
	declarations: [MeteoComponent],
	imports: [Component],
	exports: [MeteoComponent]
})
export class ComponentsModule {}
